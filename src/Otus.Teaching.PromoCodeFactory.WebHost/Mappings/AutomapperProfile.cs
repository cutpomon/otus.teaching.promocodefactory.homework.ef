﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappings
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Customer, CustomerResponse>();

            CreateMap<Customer, CustomerShortResponse>();

            CreateMap<PromoCode, PromoCodeShortResponse>();

            CreateMap<CreateOrEditCustomerRequest, Customer>();

            CreateMap<Preference, PreferenceResponse>();
        }
    }
}
