﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения.
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : ControllerBase
    {
        private readonly IRepository<Preference> _preferences;
        private readonly IMapper _mapper;

        public PreferencesController(
            IRepository<Preference> preferences,
            IMapper mapper)
        {
            _preferences = preferences;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить список предпочтений.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<PreferenceResponse>> GetPreferences()
        {
            var preferences = await _preferences
                .GetAllAsync()
                .AsNoTracking()
                .ProjectTo<PreferenceResponse>(_mapper.ConfigurationProvider)
                .ToListAsync();

            return Ok(preferences);
        }

        /// <summary>
        /// Получить предпочтение по id.
        /// </summary>
        /// <param name="id">Id предпочтения.</param>
        /// <returns></returns>
        [HttpGet("{id:guid:required}")]
        public async Task<ActionResult<PreferenceResponse>> GetPreference(Guid id)
        {
            var preference = await _preferences
                .GetByIdAsync(id)
                .AsNoTracking()
                .ProjectTo<PreferenceResponse>(_mapper.ConfigurationProvider)
                .SingleOrDefaultAsync()
                ?? throw new NotFoundException($"Предпочтение с id = {id} не найдено");

            return Ok(preference);
        }
    }
}
