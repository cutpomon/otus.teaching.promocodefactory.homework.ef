﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IMapper _mapper;

        public CustomersController(
            ICustomerRepository customerRepository,
            IMapper mapper)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные о всех клиентах
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository
                .GetAllAsync()
                .AsNoTracking()
                .ProjectTo<CustomerShortResponse>(_mapper.ConfigurationProvider)
                .ToListAsync();

            return Ok(customers);
        }

        /// <summary>
        /// Получить данные о клиенте
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <returns></returns>
        [HttpGet("{id:guid:required}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository
                .GetByIdAsync(id)
                .AsNoTracking()
                .Include(p => p.Preferences)
                .Include(p => p.PromoCodes)
                .ProjectTo<CustomerResponse>(_mapper.ConfigurationProvider)
                .SingleOrDefaultAsync()
                ?? throw new NotFoundException($"Клиент (id = {id}) не найден");

            return Ok(customer);
        }

        /// <summary>
        /// Добавить клиента со списком предпочтений.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            return await _customerRepository
                .CreateCustomerWithPreferences(_mapper.Map<Customer>(request), request.PreferenceIds);
        }

        /// <summary>
        /// Обновить клиента и его список предпочтений.
        /// </summary>
        /// <param name="id">Id клиента.</param>
        /// <param name="request"></param>
        /// <returns></returns>

        [HttpPut("{id:guid:required}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            await _customerRepository
                .UpdateCustomerWithPreferences(id, _mapper.Map<Customer>(request), request.PreferenceIds);

            return Ok();
        }

        /// <summary>
        /// Удалить клиента.
        /// </summary>
        /// <param name="id">Id клиента.</param>

        [HttpDelete("{id:guid:required}")]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            await _customerRepository.DeleteCustomerWithPreviousPromocodes(id);

            return Ok();
        }
    }
}