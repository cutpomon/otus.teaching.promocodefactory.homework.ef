﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly MySqlDbContext _context;

        public EfRepository(MySqlDbContext context)
        {
            _context = context;
        }

        public IQueryable<T> GetAllAsync() => _context.Set<T>();

        public IQueryable<T> GetByIdAsync(Guid id) => _context.Set<T>().Where(x => x.Id == id);

        public async Task<Guid> AddAsync(T entity)
        {
            await _context.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity.Id;
        }

        public async Task UpdateAsync(T entity)
        {
            _ = await _context.Set<T>().FindAsync(entity.Id)
                ?? throw new NotFoundException($"Сущность {nameof(T)} с id = {entity.Id} не найдена");

            _context.Set<T>().Update(entity);
            await _context.SaveChangesAsync();

            return;
        }

        public async Task DeleteAsync(Guid id)
        {
            var entity = await _context.Set<T>().FindAsync(id)
                ?? throw new NotFoundException($"Сущность {nameof(T)} с id = {id} не найдена");

            _context.Set<T>().Remove(entity);
            await _context.SaveChangesAsync();

            return;
        }

    }
}
