﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : EfRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(MySqlDbContext context) : base(context)
        {
        }


        public async Task<Guid> CreateCustomerWithPreferences(
            Customer entity, 
            IEnumerable<Guid> customerPreferenceIds)
        {
            var preferences = await _context.Preferences
                .Where(x => customerPreferenceIds.Contains(x.Id))
                .ToListAsync();

            foreach (var preference in preferences)
            {
                entity.Preferences.Add(preference);
            }
                
            await _context.Customers.AddAsync(entity);
            await _context.SaveChangesAsync();

            return entity.Id;
        }

        public async Task UpdateCustomerWithPreferences(
            Guid id,
            Customer entity, 
            IEnumerable<Guid> customerPreferenceIds)
        {
            var entityToUpdate = await this
                .GetByIdAsync(id)
                .SingleOrDefaultAsync()
                ?? throw new NotFoundException($"Клиент с id = {id} не найден");

            var preferences = await _context.Preferences
                .Where(x => customerPreferenceIds.Contains(x.Id))
                .ToListAsync();

            entityToUpdate.Email = entity.Email;
            entityToUpdate.FirstName = entity.FirstName;
            entityToUpdate.LastName = entity.LastName;

            await _context.CustomerPreferences
                .Where(x => x.CustomerId == id)
                .ExecuteDeleteAsync();

            foreach(var preference in preferences)
            {
                entityToUpdate.Preferences.Add(preference);
            }

            await _context.SaveChangesAsync();

            return;
        }

        public async Task DeleteCustomerWithPreviousPromocodes(Guid id)
        {
            await DeleteAsync(id);

            var promoCodesToDelete = await _context.PromoCodes
                .Where(x => x.CustomerId == id)
                .ToListAsync();

            _context.PromoCodes.RemoveRange(promoCodesToDelete);

            await _context.SaveChangesAsync();

            return;
        }
    }
}
