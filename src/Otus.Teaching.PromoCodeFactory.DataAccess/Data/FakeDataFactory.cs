﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleId = Roles.FirstOrDefault(x => x.Name == "Admin").Id,
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                RoleId = Roles.FirstOrDefault(x => x.Name == "PartnerManager").Id,
                AppliedPromocodesCount = 10
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };
        
        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };

        public static IEnumerable<PromoCode> PromoCodes => new List<PromoCode>
        {
            new PromoCode()
            {
                Id = Guid.Parse("05a92e84-76ce-41a7-a943-de92d5cdf4db"),
                Code = "de92d5cdf4db",
                BeginDate = new DateTime(2023, 1, 1),
                EndDate = new DateTime(2023, 1, 10),
                PartnerName = "Otus",
                ServiceInfo = "Education service",
                CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                PartnerManagerId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c")
            },
            new PromoCode()
            {
                Id = Guid.Parse("6d32a3b1-6d5a-4681-976c-65daba543d2e"),
                Code = "65daba543d2e",
                BeginDate = new DateTime(2023, 2, 1),
                EndDate = new DateTime(2023, 1, 10),
                PartnerName = "Big theatre",
                ServiceInfo = "Culture service",
                CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                PartnerManagerId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c")
            },
            new PromoCode()
            {
                Id = Guid.Parse("af685f51-a2ad-4f89-99fd-6446a260914f"),
                Code = "6446a260914f",
                BeginDate = new DateTime(2023, 3, 1),
                EndDate = new DateTime(2023, 3, 10),
                PartnerName = "Child world",
                ServiceInfo = "Entertainment service",
                CustomerId = Guid.Parse("f28846ee-67b5-4220-90d7-1c84c8321413"),
                PartnerManagerId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                PreferenceId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84")
            }
        };


        public static IEnumerable<Customer> Customers
        {
            get
            {
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров"
                    },
                    new Customer()
                    {
                        Id = Guid.Parse("f28846ee-67b5-4220-90d7-1c84c8321413"),
                        Email = "arkadiy_borisov@mail.ru",
                        FirstName = "Аркадий",
                        LastName = "Борисов"
                    }
                };

                return customers;
            }
        }

        public static IEnumerable<CustomerPreference> CustomerPreferences
        {
            get
            {
                var customers = new List<CustomerPreference>
                {
                    new CustomerPreference()
                    {
                        CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c")
                    },
                    new CustomerPreference()
                    {
                        CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        PreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd")
                    },
                    new CustomerPreference()
                    {
                        CustomerId = Guid.Parse("f28846ee-67b5-4220-90d7-1c84c8321413"),
                        PreferenceId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84")
                    }
                };

                return customers;
            }
        }
    }
}