﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DbInitializer : IDbInitializer
    {
        private readonly MySqlDbContext _context;

        public DbInitializer(MySqlDbContext dataContext)
        {
            _context = dataContext;
        }

        public async Task InitializeDb()
        {
            await _context.Database.EnsureDeletedAsync();
            await _context.Database.EnsureCreatedAsync();

            await _context.AddRangeAsync(FakeDataFactory.Roles);
            await _context.AddRangeAsync(FakeDataFactory.Employees);
            await _context.AddRangeAsync(FakeDataFactory.Preferences);
            await _context.AddRangeAsync(FakeDataFactory.Customers);
            await _context.AddRangeAsync(FakeDataFactory.PromoCodes);
            await _context.AddRangeAsync(FakeDataFactory.CustomerPreferences);

            await _context.SaveChangesAsync();
        }
    }
}
