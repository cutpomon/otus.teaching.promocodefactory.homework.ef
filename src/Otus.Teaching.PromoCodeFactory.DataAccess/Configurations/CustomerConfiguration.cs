﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Configuration
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder
                .HasKey(e => e.Id);

            builder
                .Ignore(p => p.FullName);

            builder
                .HasMany(e => e.PromoCodes)
                .WithOne(e => e.Customer)
                .HasForeignKey(f => f.CustomerId);

            builder
                .HasMany(p => p.Preferences)
                .WithMany(p => p.Customers)
                .UsingEntity<CustomerPreference>(
                    f => f
                        .HasOne(x => x.Preference)
                        .WithMany(y => y.CustomerPreferences)
                        .HasForeignKey(x => x.PreferenceId),
                    f => f
                        .HasOne(x => x.Customer)
                        .WithMany(x => x.CustomerPreferences)
                        .HasForeignKey(x => x.CustomerId),
                    f =>
                    {
                        f.HasKey(k => new { k.CustomerId, k.PreferenceId });
                    });

        }
    }
}
