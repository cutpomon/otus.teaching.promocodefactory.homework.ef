﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        Task<Guid> CreateCustomerWithPreferences(
            Customer entity, 
            IEnumerable<Guid> customerPreferenceIds);

        Task UpdateCustomerWithPreferences(
            Guid id,
            Customer entity, 
            IEnumerable<Guid> customerPreferenceIds);

        Task DeleteCustomerWithPreviousPromocodes(Guid id);
    }
}
