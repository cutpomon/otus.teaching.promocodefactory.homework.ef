﻿using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions
{
    public interface IDbInitializer
    {
        Task InitializeDb();
    }
}
