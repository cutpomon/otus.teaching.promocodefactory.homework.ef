﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode : BaseEntity
    {
        [MaxLength(40)]
        public string Code { get; set; }

        [MaxLength(200)]
        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        [MaxLength(50)]
        public string PartnerName { get; set; }

        [ForeignKey(nameof(Employee))]
        public Guid PartnerManagerId { get; set; }
        public Employee PartnerManager { get; set; }

        [ForeignKey(nameof(Preference))]
        public Guid PreferenceId { get; set; }
        public Preference Preference { get; set; }

        [ForeignKey(nameof(Customer))]
        public Guid CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}