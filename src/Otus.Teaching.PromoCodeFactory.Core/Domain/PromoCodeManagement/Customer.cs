﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer : BaseEntity
    {
        [MaxLength(30)]
        public string FirstName { get; set; }

        [MaxLength(30)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(100)]
        public string Email { get; set; }

        public ICollection<CustomerPreference> CustomerPreferences { get; set; } = new List<CustomerPreference>();

        public ICollection<Preference> Preferences { get; set; } = new List<Preference>();

        public ICollection<PromoCode> PromoCodes { get; set; } = new List<PromoCode>();
    }
}