﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPreference : BaseEntity
    {
        [ForeignKey(nameof(Customer))]
        public Guid CustomerId { get; set; }
        public Customer Customer { get; set; }

        [ForeignKey(nameof(Preference))]
        public Guid PreferenceId { get; set; }
        public Preference Preference { get; set; }
    }
}
